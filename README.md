Frontend Checkers
===================
Npm package that contain eslint and scss-lint.

Usage eslint
===================
In order to start using this package, you need to add it to 
package.json
```
"frontend_checkers": "git+http://odemchenko@bitbucket.exist.ua/scm/com/frontend_checkers.git"
```
and run: 
```
npm install
```
After installation need to add - "frontend_checkers" to your`s
.eslintrc

```
"extends": "frontend_checkers"
``` 
Usage scss-lint
===================
```
gem install scss_lint
scss-lint node_modules/frontend_checkers/.scss-lint.yml
```