module.exports = {
    rules: {
        "global-require": 0,
        "no-param-reassign": ["error", { "props": false }],
        "react/no-danger": 0,
        "no-debugger": 0,
        "no-console": 0,
        "semi": 2,
        "quotes": [0, "single"],
        "indent": ["error", 4],
        "react/jsx-indent": 0,
        "react/jsx-indent-props": 0,
        "react/jsx-filename-extension": 0,
        "jsx-quotes": [1, "prefer-single"],
        "react/jsx-uses-react": 1,
        "react/jsx-uses-vars": 1
    }
};